import glob
import os
import random
import shutil
import configparser
import pathlib
from redditDownloader import RedditDownloader
from mastUpload import MastUpload
from imageResize import ImageResizer
from twitterUpload import TwitterUpload
from pixivDownloader import PixivDownloader

#https://medium.com/avalanche-of-sheep/simple-twitter-image-bot-in-python-6e4a7f2dc4af

os.chdir("/home/mateusfelipecota/KyaruBot/")

def isNSFW(fileName):
    details = filename.split('_')
    if(details[0] == "NSFW"):
        return True
    else:
        return False

def getImageSource(fileName):
    details = fileName.split('_')
    content = 'Source: '
    if(details[0] == 'illust'):
        content += 'https://www.pixiv.net/member_illust.php?mode=medium&illust_id=' + details[2]
        return content
    elif (len(fileName.split('-')) >= 2): #imgur
        details = fileName.split('-')
        id = details[1].strip()
        id = id.split('.')
        content += 'https://imgur.com/'+id[0]
        return content
    else:
        content += 'https://redd.it/'+ details[1]
        return content

def imagePicker(folder):
    images = glob.glob(folder+"*")
    image_open = images[random.randint(0,len(images))-1]
    return image_open

def fileMover(filepath):
    shutil.move(filepath, uploaded+os.path.basename(filepath))

#import pathlib
#pathlib.Path(file).suffix[1:]

ignoreTypes = ['gif','webm','mp4']

if __name__ == "__main__":
    #File paths
    original = os.getcwd()+'/images/'
    uploaded = os.getcwd()+'/uploaded/'
    
    #image downloading
    # reddit = RedditDownloader('rarekumikos')
    # reddit = RedditDownloader('hibikeeuphonium')
    
    # pixiv downloading
    # tags = ['hibike euphonium','響け! ユーフォニアム']
    # pixiv = PixivDownloader()
    # pixiv.download(tags)
    
    #FilePicking and resizing
    file = imagePicker(original)

    #get the file name
    filename = os.path.basename(file)

    #source = getImageSource(filename)

    # This makes that the PIL function doesn't convert any video file
    # Or any extension inside ignoreTypes variable
    if (pathlib.Path(file).suffix[1:] in ignoreTypes == False):
        resizer = ImageResizer()
        resizer.resize(file)

    #Posting on mastodon
    if(isNSFW(file)):
        mastodon = MastUpload()
        mastodon.postFileNSFW(file)
    else:
        mastodon = MastUpload()
        mastodon.postFile(file)

    #Posting on twitter
    # twitter = TwitterUpload()
    # twitter.postMedia(file)

    #taking files of the pool
    #fileMover(file)
