from pixivpy3 import *
import os
import json
import configparser

class PixivCrawler(object):
    
    api = type(PixivAPI)
    dir = ""
    def getConfig(self):
        config = configparser.ConfigParser()
        config.read("config.ini")

        return config['PIXIV']

    def __init__(self):

        pixivConfig = self.getConfig()

        self.api = PixivAPI()
        
        self.api.login(pixivConfig['USERNAME'],pixivConfig['PASSWORD'])
        self.dir = (os.getcwd()+"/images/")
    # json_results = api.illust_ranking('day')
    # json_results = api.search_works("hibike euphonium");
    
    def download(self, tags):
        for tag in tags:
            json_results = self.api.search_works(tag,per_page=120)
            print(json_results.pagination.total)
            if(json_results.status == 'success'):
                for i in range(0, json_results.pagination.pages):
                    if(i != 0):
                        json_results = self.api.search_works(tag,per_page=120,page=i)
                    if(json_results.status == 'success'):
                        for images in json_results.response:
                            #print(images.image_urls.large)
                            os.chdir(self.dir)
                            self.api.download(images.image_urls.large)
                    else:
                        print("failure")
            else:
                print("failure")
