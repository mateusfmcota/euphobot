from PIL import Image
from resizeimage import resizeimage


class ImageResizer(object):
    def __init__(self):
        x = 0

    def resize(self, filepath):
        fd_img = open(filepath, 'r+b')
        img = Image.open(fd_img)
        
        width, height = img.size
        
        if(width > 1080 or height > 1080):
            img = type(resizeimage)
            if(width > height):
                img = resizeimage.resize_width(img, 1080)
            else:
                img = resizeimage.resize_height(img, 1080)
            img.save(filepath, img.format)

        fd_img.close()
