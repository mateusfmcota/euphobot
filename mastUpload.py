import configparser
import os
from mastodon import Mastodon

class MastUpload(object):

    mastodon = type(Mastodon)

    def __init__(self):
        # loading configFile
        #print('teste')
        config = configparser.ConfigParser()
        config.read('config.ini')

        mastodonConfigs = config['MASTODON']
        #print(mastodonConfigs['TOKEN'])
        #print(mastodonConfigs['API_URL'])

        self.mastodon = Mastodon(access_token = 'kuarubotsec.secret', api_base_url = 'https://ihatebeinga.live')
       # mastodon.log_in(mastodonConfigs['USERNAME'], mastodonConfigs['PASSWORD'], to_file = 'kyarubot.secret')

    def postFileContent(self, filepath, contentSource):
        media = self.mastodon.media_post(filepath)
        self.mastodon.status_post(contentSource,media_ids=media,visibility="unlisted")

    def postFile(self, filepath):
        media = self.mastodon.media_post(filepath)
        self.mastodon.status_post(media_ids=media,status='.',visibility="unlisted")

    def postFileNSFW(self, filepath):
        media = self.mastodon.media_post(filepath)
        self.mastodon.status_post(media_ids=media,status='.',sensitive=True,visibility="unlisted")
