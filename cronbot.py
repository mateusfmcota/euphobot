from crontab import CronTab

cron = CronTab(user='mateusfelipecota')
job = cron.new(command='python3 /home/mateusfelipecota/kumikobot/euphobot.py')
job.minute.every(1)

cron.write()
