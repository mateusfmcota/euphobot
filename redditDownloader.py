import praw
import urllib
import sys
import os
import signal
import configparser
from prawcore import NotFound
from prawcore import PrawcoreException
from docopt import docopt

class RedditDownloader(object):
    def __init__(self, subreddit):

        #configLoad
        config = configparser.ConfigParser()
        config.read('config.ini')

        redditConfigs = config['REDDIT']

        # connect to reddit
        reddit = praw.Reddit(
            client_id=redditConfigs['ID'],
            client_secret=redditConfigs['SECRET'],
            password=redditConfigs['PASSWORD'],
            user_agent=redditConfigs['AGENT'],
            username=redditConfigs['USERNAME'])

        if not os.path.exists('./images'):
            os.mkdir('./images')

        lastPostId = redditConfigs[subreddit]
        #print(subreddit)
        #print(lastPostId)
        postIdChanged = False
        newLastPostId = ''

        results = reddit.subreddit(subreddit).new()

        # find images/gifs in subreddit
        try:
            count = 1
            for submission in results:
                if (submission.stickied == False and postIdChanged == False):
                    postIdChanged = True
                    newLastPostId = submission.id
                if not (submission == lastPostId):
                    if 'https://i.imgur.com/' in submission.url or 'https://i.redd.it' in submission.url:
                        img_url = submission.url
                        _, extension = os.path.splitext(img_url)
                        if extension in ['.jpg', '.gif', '.jpeg', '.png']:
                            urllib.request.urlretrieve(img_url, 'images/%s_%s_%s' %
                                               (subreddit, submission.id, extension))
                            count += 1
                        # .gifv file extensions do not play, convert to .gif
                        elif extension == '.gifv':
                            root, _ = os.path.splitext(img_url)
                            img_url = root + '.gif'
                            urllib.request.urlretrieve(img_url, 'images/%s_%s_%s' %
                                               (subreddit, submission.id, '.gif'))
                            count += 1
                    if 'https://thumbs.gfycat.com/' in submission.url:
                        img_url = submission.url
                        urllib.request.urlretrieve(img_url, 'images/%s_%s_%s' %
                                           (subreddit, submission.id, '.gif'))
                        count += 1
                    # some gfycat conversions will not work due to capitalizations of link
                    if 'https://gfycat.com/' in submission.url:
                        img_url = submission.url
                        img_url = img_url.split('https://', 1)
                        img_url = 'https://thumbs.' + img_url[1]
                        if 'gifs/detail/' in img_url:
                            img_url = img_url.split('gifs/detail/', 1)
                            img_url = img_url[0] + img_url[1]
                        root, _ = os.path.splitext(img_url)
                        img_url = root + '-size_restricted.gif'
                        urllib.request.urlretrieve(img_url, 'images/%s_%s_%s' %
                                           (subreddit, submission.id, '.gif'))
                        count += 1
                else:
                    redditConfigs[subreddit] = newLastPostId
                    with open('config.ini', 'w') as configfile:
                          config.write(configfile)

                    break


        except PrawcoreException:
            print ('\nError accessing subreddit!\n')

