import configparser
import tweepy 

class TwitterUpload(object):

    API = type(tweepy.API())
    def getConfig(self):
        config = configparser.ConfigParser()
        config.read('config.ini')        

        return config['TWITTER']

    def twitterAPI(self):
        twitterConfigs = self.getConfig()
        
        #authentication process
        auth = tweepy.OAuthHandler(twitterConfigs["CONSUMER_KEY"],twitterConfigs["CONSUMER_SECRETS"])
        auth.set_access_token(twitterConfigs["ACCESS_TOKEN"], twitterConfigs["ACCESS_TOKEN_SECRET"])
        return tweepy.API(auth)

    def postMediaContent(self, filename, contentSource):
        self.API.update_with_media(filename= filename, status= contentSource)

    def postMedia(self, filename):
        self.API.update_with_media(filename= filename)

    def __init__(self):
        self.API = self.twitterAPI()