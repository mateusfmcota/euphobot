# kumikobot
    > This is a bot that posts every hour a image/gif on twitter and mastodon

    > To run you need to set-up the config.ini file and the main folder on euphobot.py and create a cronjob that will run every x time to post the image on social medias


#Reference links
https://gist.github.com/zmwangx/6191892dc6a96baebdce
https://github.com/wodim/akari/blob/master/akari.py
https://github.com/wodim/akari/blob/master/telegram_bot.py
https://stackabuse.com/scheduling-jobs-with-python-crontab/
https://medium.com/avalanche-of-sheep/simple-twitter-image-bot-in-python-6e4a7f2dc4af
